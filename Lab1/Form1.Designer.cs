﻿namespace Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ChartMixed = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelData = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.ChartHarmonics = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.ChartMixed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartHarmonics)).BeginInit();
            this.SuspendLayout();
            // 
            // ChartMixed
            // 
            chartArea1.Name = "ChartArea1";
            this.ChartMixed.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.ChartMixed.Legends.Add(legend1);
            this.ChartMixed.Location = new System.Drawing.Point(12, 12);
            this.ChartMixed.Name = "ChartMixed";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series";
            this.ChartMixed.Series.Add(series1);
            this.ChartMixed.Size = new System.Drawing.Size(1107, 300);
            this.ChartMixed.TabIndex = 0;
            this.ChartMixed.Text = "Chart";
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Location = new System.Drawing.Point(12, 332);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(0, 13);
            this.labelData.TabIndex = 1;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(566, 655);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // ChartHarmonics
            // 
            chartArea2.Name = "ChartArea1";
            this.ChartHarmonics.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.ChartHarmonics.Legends.Add(legend2);
            this.ChartHarmonics.Location = new System.Drawing.Point(12, 348);
            this.ChartHarmonics.Name = "ChartHarmonics";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series";
            this.ChartHarmonics.Series.Add(series2);
            this.ChartHarmonics.Size = new System.Drawing.Size(1107, 300);
            this.ChartHarmonics.TabIndex = 0;
            this.ChartHarmonics.Text = "Chart";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 690);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.labelData);
            this.Controls.Add(this.ChartHarmonics);
            this.Controls.Add(this.ChartMixed);
            this.Name = "Form1";
            this.Text = "Lab1";
            ((System.ComponentModel.ISupportInitialize)(this.ChartMixed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartHarmonics)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart ChartMixed;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartHarmonics;
    }
}

