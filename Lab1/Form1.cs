﻿using Lib;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        private readonly Generator _generator = new Generator();
        private readonly Stopwatch _stopwatch = new Stopwatch();

        public Form1()
        {
            InitializeComponent();

            GenerateSignals();
        }

        private void GenerateSignals()
        {
            _stopwatch.Reset();
            _stopwatch.Start();

            var harmonics = _generator.GenerateHarmonics();
            var res = _generator.MixHarmonics(harmonics);
            var m = _generator.CalculateMathematicalExpectation(res);
            var d = _generator.CalculateDispersion(res, m);

            _stopwatch.Stop();

            //labelData.Text = $"M: {m}, D: {d}, Elapsed: {_stopwatch.ElapsedTicks} ticks";

            ChartMixed.DrawSignal(res);
            ChartHarmonics.DrawHarmonics(harmonics);
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            GenerateSignals();
        }
    }
}
