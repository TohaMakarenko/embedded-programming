using System;
using System.Linq;

namespace Common
{
    public class Generator
    {
        private const int N = 64;
        private const int n = 12;
        private const int w = 1200;

        private readonly Random _random;

        public Generator()
        {
            _random = new Random();
        }

        public double[][] GenerateHarmonics()
        {
            var res = new double[n][];

            for (var i = 0; i < n; i++)
            {
                res[i] = GenerateHarmonic(N, w * (i + 1) * 1.0 / n);
            }

            return res;
        }

        private double[] GenerateHarmonic(int N, double W)
        {
            var res = new double[N];

            var a = _random.NextDouble() * 2 + 2;
            var fi = _random.NextDouble() * 2 * Math.PI;

            for (var i = 0; i < N; i++)
            {
                res[i] = a * Math.Sin(W * i + fi);
            }

            return res;
        }

        public double[] MixHarmonics(double[][] harmonics)
        {
            var result = new double[harmonics[0].Length];

            foreach (var harmonic in harmonics)
            {
                for (var i = 0; i < harmonic.Length; i++)
                {
                    result[i] += harmonic[i];
                }
            }

            return result;
        }

        public double CalculateMathematicalExpectation(double[] signal)
        {
            return signal.Sum() / signal.Length;
        }

        public double CalculateDispersion(double[] signal, double mathematicalExpectation)
        {
            return signal.Sum(s =>
                       (s - mathematicalExpectation) *
                       (s - mathematicalExpectation))
                   / (signal.Length - 1);
        }

        public double[] GetMathematicalExpectationsPerTimeline(double[][] harmonics)
        {
            var mathExpectations = new double[harmonics.First().Length];

            for (var i = 0; i < mathExpectations.Length; i++)
            {
                mathExpectations[i] = CalculateMathematicalExpectation(harmonics.Select(h => h[i]).ToArray());
            }

            return mathExpectations;
        }

        public double CalculateAutocorrelation(double[][] harmonics)
        {
            const int tau = 1;

            var mathExpectations = GetMathematicalExpectationsPerTimeline(harmonics);

            var res = 0d;

            for (int t = 0; t < harmonics.Length - tau; t++)
            {
                foreach (var harmonic in harmonics)
                {
                    res += harmonic[t] * mathExpectations[t] * (harmonic[t + tau] * mathExpectations[t + tau]);
                }
            }

            return res / (n - 1);
        }

        public double CalculateAutocorrelation(double[] signal)
        {
            const int tau = 2;

            var mathExpectations = CalculateMathematicalExpectation(signal);

            var res = 0d;

            for (int t = 0; t < signal.Length - tau; t++)
            {
                res += signal[t] * mathExpectations * (signal[t + tau] * mathExpectations);
            }

            return res / (N - 1);
        }

        public double CalculateCorrelationBetweenSignals(double[] signalX, double[] signalY)
        {
            const int tau = 2;

            var mx = CalculateMathematicalExpectation(signalX);
            var my = CalculateMathematicalExpectation(signalY);

            var res = 0d;

            for (var i = 0; i < N - tau; i++)
            {
                res += (signalX[i] - mx) * (signalY[i + tau] * my);
            }

            return res / (N - 1);
        }
    }
}