﻿using System.Data;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Common
{
    public static class ChartExtensions
    {
        private const string XValues = nameof(XValues);
        private const string YValues = nameof(YValues);
        private const string SeriesName = "Series";

        public static void DrawSignal(this Chart chart, double[] signal)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add(XValues, typeof(double));
            dataTable.Columns.Add(YValues, typeof(double));

            for (var i = 0; i < signal.Length; i++)
            {
                dataTable.Rows.Add(i, signal[i]);
            }

            chart.DataSource = dataTable;
            chart.Series[SeriesName].XValueMember = XValues;
            chart.Series[SeriesName].YValueMembers = YValues;
            chart.Series[SeriesName].ChartType = SeriesChartType.Line;

            chart.ChartAreas[0].AxisY.LabelStyle.Format = "";
            chart.ChartAreas[0].AxisX.Interval = 4;
        }

        public static void DrawHarmonics(this Chart chart, double[][] harmonics)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add(XValues, typeof(double));

            var columnsAndSeries = harmonics
                .Select((v, i) => (ColumnName: YValues + i, SeriesName: SeriesName + i))
                .ToList();

            foreach (var value in columnsAndSeries)
            {
                dataTable.Columns.Add(value.ColumnName, typeof(double));
            }

            var harmonicLength = harmonics.First().Length;

            for (var i = 0; i < harmonicLength; i++)
            {
                var values = harmonics.Select(h => h[i]).Prepend(i).Cast<object>().ToArray();
                dataTable.Rows.Add(values);
            }

            chart.DataSource = dataTable;

            if (chart.Series.Any())
                chart.Series.Clear();

            foreach (var value in columnsAndSeries)
            {
                chart.Series.Add(value.SeriesName);

                chart.Series[value.SeriesName].XValueMember = XValues;
                chart.Series[value.SeriesName].YValueMembers = value.ColumnName;
                chart.Series[value.SeriesName].ChartType = SeriesChartType.Line;
            }

            chart.ChartAreas[0].AxisY.LabelStyle.Format = "";
            chart.ChartAreas[0].AxisX.Interval = 4;
        }
    }
}
