﻿using Lib;
using System;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        private const string AutocorrelationOfSignalTitle = "Autocorrelation of signal";
        private const string CorrelationWithTheCopyOfSignal = "Correlation with the copy of signal";

        private readonly Generator _generator = new Generator();
        
        public Form1()
        {
            InitializeComponent();
        }

        public void Update()
        {
            var harmonics = _generator.GenerateHarmonics();
            var signal = _generator.MixHarmonics(harmonics);

            harmonics = _generator.GenerateHarmonics();
            var copyOfSignal = _generator.MixHarmonics(harmonics);

            var autocorrelationOfSignal = _generator.CalculateAutocorrelation(signal);
            var correlationWithCopy = _generator.CalculateCorrelationBetweenSignals(signal, copyOfSignal);

            labelAutocorrelation.Text = $"{AutocorrelationOfSignalTitle}: {autocorrelationOfSignal}";
            labelCorrelationWithCopy.Text = $"{CorrelationWithTheCopyOfSignal}: {correlationWithCopy}";

            chartSignal.DrawHarmonics(new double[][] { signal, copyOfSignal});
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            Update();
        }
    }
}
