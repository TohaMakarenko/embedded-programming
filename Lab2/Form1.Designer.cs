﻿namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartSignal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelAutocorrelation = new System.Windows.Forms.Label();
            this.labelCorrelationWithCopy = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartSignal)).BeginInit();
            this.SuspendLayout();
            // 
            // chartSignal
            // 
            chartArea1.Name = "ChartArea1";
            this.chartSignal.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartSignal.Legends.Add(legend1);
            this.chartSignal.Location = new System.Drawing.Point(12, 12);
            this.chartSignal.Name = "chartSignal";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series";
            this.chartSignal.Series.Add(series1);
            this.chartSignal.Size = new System.Drawing.Size(1165, 510);
            this.chartSignal.TabIndex = 0;
            this.chartSignal.Text = "Chart";
            // 
            // labelAutocorrelation
            // 
            this.labelAutocorrelation.AutoSize = true;
            this.labelAutocorrelation.Location = new System.Drawing.Point(9, 525);
            this.labelAutocorrelation.Name = "labelAutocorrelation";
            this.labelAutocorrelation.Size = new System.Drawing.Size(126, 13);
            this.labelAutocorrelation.TabIndex = 1;
            this.labelAutocorrelation.Text = "Autocorrelation of signal: ";
            // 
            // labelCorrelationWithCopy
            // 
            this.labelCorrelationWithCopy.AutoSize = true;
            this.labelCorrelationWithCopy.Location = new System.Drawing.Point(9, 555);
            this.labelCorrelationWithCopy.Name = "labelCorrelationWithCopy";
            this.labelCorrelationWithCopy.Size = new System.Drawing.Size(171, 13);
            this.labelCorrelationWithCopy.TabIndex = 1;
            this.labelCorrelationWithCopy.Text = "Correlation with the copy of signal: ";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(10, 587);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 628);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.labelCorrelationWithCopy);
            this.Controls.Add(this.labelAutocorrelation);
            this.Controls.Add(this.chartSignal);
            this.Name = "Form1";
            this.Text = "Lab2";
            ((System.ComponentModel.ISupportInitialize)(this.chartSignal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartSignal;
        private System.Windows.Forms.Label labelAutocorrelation;
        private System.Windows.Forms.Label labelCorrelationWithCopy;
        private System.Windows.Forms.Button buttonUpdate;
    }
}

