﻿using Lib;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab3
{
    public partial class Form1 : Form
    {
        private readonly Generator _generator = new Generator();

        public Form1()
        {
            InitializeComponent();
            Update();
        }

        private void Update()
        {
            var harmonics = _generator.GenerateHarmonics();
            var signal = _generator.MixHarmonics(harmonics);
            var dft = _generator.DFT(signal);
            var fdft = _generator.FDFT(signal.Select(x => new complex(x, 0)).ToArray()).Select(x => x.magnitude).ToArray();

            chartSignal.DrawSignal(signal);
            chartDFT.DrawSignal(dft);
            chartFDFT.DrawSignal(fdft);
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            Update();
        }
    }
}
